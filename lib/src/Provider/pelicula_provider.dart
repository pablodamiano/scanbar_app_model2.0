import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:model_barcode_app/src/Model/actores_model.dart';

class PeliculasProvider {

  String _apikey = '8ed93b3b73646150b89042d1bcb029ab';
  String _url = 'api.themoviedb.org';
  Future<List<Actor>> getCast(String peliID)async{

    final url = Uri.https(_url,'3/movie/$peliID/credits',{
      'api_key': _apikey,
    });
    print(url);
    final resp = await http.get(url);
    final decodeData = json.decode(resp.body);

    final cast = new Cast.fromJsonList(decodeData['cast']);
    return cast.actores;
    
  }

}