import 'dart:async';

import 'package:flutter/material.dart';
import 'package:model_barcode_app/src/Provider/pelicula_provider.dart';



class BarcodePage extends StatefulWidget {
  @override
  _BarcodePageState createState() => _BarcodePageState();
}

class _BarcodePageState extends State<BarcodePage> {
  String nombre = '';
  String nombre2 = '';
  String valorid = '';
  String valorid2 = '';
  bool actScaner = false;
  List<Widget> list = [];
  final peliProvider = PeliculasProvider();
  var _controller = TextEditingController();
  @override
  Widget build(BuildContext context) {

  if(actScaner == true){ valorid = nombre;}
  if(actScaner == false){ valorid2 = nombre;}

    return Scaffold(
      appBar:AppBar(
        backgroundColor: Colors.green,
        title: Text('BarcodePage'),
      ),
      body: SingleChildScrollView(
          child: Container(
          child: Center(
            child:Column(
              mainAxisAlignment:MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: 40.0,),
                 Row(
                   mainAxisAlignment: MainAxisAlignment.center,
                   children: <Widget>[
                     crearboton('Scaner_On',(){setState(() {actScaner = true;}); }, (actScaner==true)?Colors.green:Colors.blueGrey[300]),
                     SizedBox(width: 50.0,),
                     crearboton('Scaner_Of',(){setState(() {actScaner = false;});}, (actScaner==false)?Colors.green:Colors.blueGrey[300]),
                   ],
                 ),
                 SizedBox(height: 40.0,),
                 _crearInput(actScaner),
                 Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                     _botonService(),
                      SizedBox(width: 50.0,),
                     crearboton('ClearList',(){setState(() {list.clear();});},Colors.grey[300]),
                  ],
                 ),
                 Column(
                  children:list,
                 ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _crearInput(bool actScaner) {

   return TextField(
     focusNode: FocusNode(),
     controller: _controller,
     enabled: true,
     autofocus: true,
     textCapitalization: TextCapitalization.sentences,
     decoration: InputDecoration(
       border: OutlineInputBorder(
         borderRadius: BorderRadius.circular(20.0),
       ),
       hintText:'Code Bar',
       helperText:'Digital Bar Code',
       suffixIcon: Icon(Icons.accessibility_new),
       icon: Icon(Icons.account_circle),
       labelText: 'BarCode',
     ),
     onSubmitted: (value){
        Timer(Duration(milliseconds:700),(){
        setState(() {
         nombre = value;
        });
        });
       
        if(actScaner == true){
          list.add(
             ListTile(
               title: Text(valorid),
               trailing:_consultacode(valorid),
             )
           );
           list.add(Divider());
           Timer(Duration(milliseconds: 1000),(){ _controller.clear();});
        }
     },
    onChanged: (valor){
       Timer(Duration(seconds: 0),(){
         setState(() {
         nombre = valor;
        });
       });
       
    },
   );

  }
  Widget _botonService(){
    return RaisedButton(
      onPressed:(){
      
       if(actScaner == false){
          setState(() {
            list.add(
             ListTile(
               title: Text(valorid2),
               trailing:_consultacode(valorid2),
             )
           );
           list.add(Divider());
          });
           Timer(Duration(milliseconds: 1000),(){ _controller.clear();});

        }  
      },
      child: Text('Enviar'),
      );
  }

  Widget _consultacode(String data) {
    
    return FutureBuilder(
      future: peliProvider.getCast(data),    
      initialData: [],  
      builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
        if (snapshot.data.length != 0) {
          return Icon(Icons.check,size:20,);
          
        }else{
          return Container(width:20,height: 20, child:Icon(Icons.clear));
          
        }
      },
    );
  }
  
 Widget crearboton(String title,Function function, Color color){
   return RaisedButton(
     color:color,
     onPressed:function,
     child: Text(title),
     );
 }

}